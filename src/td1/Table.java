package td1;

import java.util.Scanner;

public class Table {

	public static void main(String[] args) {
		
		int[] tab = saisieTable();
		afficheTable(tab);
		afficheTable(copieTable(tab));
		afficheTable(trierAbulle(tab));
		positionTable(tab, 6);
		
	}
	
	private static int[] trierAbulle(int[] tab)
	{
	 
		int n = tab.length;
		int temp = 0;
		 
		for(int i=0; i < n; i++)
		{
			for(int j=1; j < (n-i); j++)
			{
		 
				if(tab[j-1] < tab[j])
				{
					temp = tab[j-1];
					tab[j-1] = tab[j];
					tab[j] = temp;
				}
			}
		}
		return tab;
	}
	public static int[] saisieTable()
	{
		System.out.println("Saisir la longueur du tableau: ");
		Scanner sc = new Scanner(System.in);
		int nb = sc.nextInt();
		int[] tab = new int[nb];
		for(int i =0;i<tab.length;i++)
		{
			System.out.println("Saisir le nombre pour la position: "+i);
			tab[i] = sc.nextInt();
		}
		
		return tab;
	}
	
	public static void positionTable(int[] tab, int element)
	{
		int posElement = 0;
		boolean existe = false;
		for(int i =0;i<tab.length;i++)
		{
			if (tab[i] == element)
			{
				posElement = i;
				existe = true;
			}
		}
		if(existe)
			System.out.println("position de " +element+ " : " +posElement);
		else
			System.out.println("Cet �l�ment n'existe pas dans le tableau");
	}
	
	public static void afficheTable(int[] tab)
	{
		for(int i =0;i<tab.length;i++)
		{
			System.out.println(tab[i]);
		}
	}
	
	public static int[] copieTable(int[] tab)
	{
		int[] copieTab = new int[tab.length];
		for(int i = 0 ; i < copieTab.length ; i++)
		{
		    copieTab[i] = tab[i] ;
		}
		return copieTab;
	}

}
