package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Bonjour {

	public static void main(String[] args) throws IOException  {
		
//		exercice1();
//		exercice2();
//		exercice3();
//		exercice4();
//		exercice5();
//		exercice6();
//		saisieDouble("Saisir un nombre r�el : "); // exercice 7
//		exercice8();
		exercice9();
	}
	
	public static void exercice1()
	{
		System.out.println("Bonjour");
	}
	
	public static void exercice2() throws IOException
	{
 		//Demander le nom
 		System.out.println("Quel est votre nom ? ");
		//cr�ation d'un buffer "saisie" puis on lui ajoute InputStreamReader pour �crire des informations
		//system.in permet de saisir au clavier
 		BufferedReader saisie = new BufferedReader
 			      (new InputStreamReader(System.in));
		//on affecte  ce qui est saisie � un attribut "nom" de type string
 		String nom = saisie.readLine();
 		//Demander le pr�nom
 		System.out.println("Quel est votre pr�nom ? ");
		//d�claration d'un objet sc de type Scanner
 		Scanner sc = new Scanner(System.in);
		//on affecte ce qui est saisie � l'attribut prenom
 		String prenom = sc.next();
		//affichage de ce qui est saisie
 		System.out.println("Bonjour "+ nom + " "+ prenom);
 		sc.close();

		/* 
		* scanner est plus simple � utiliser car on � juste � instancier un objet
		* puis on affecte ce qui est saisie � une variable
		* le buffer est plus compliqu� car il faut instancier 2 objets mais c'est similaire � scanner
		* car une fois qu'on d�clare les 2 objets on affecte ce qui est saisie � une variable
		* l'avantage du buffer c'est qu'il g�re des exceptions donc c'est plus optimis� pour les longues saisies
		* l'avantage de scanner c'est qu'il est plus facile � utiliser pour des courtes saisies
		*/
	}
	
	public static void exercice3() throws IOException
	{
			//Demander le nom
			System.out.println("Quel est votre nom ? ");
			//cr�ation d'un buffer "saisie" puis on lui ajoute InputStreamReader pour �crire des informations
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			//on affecte  ce qui est saisie � un attribut "nom" de type string
			String nom = saisie.readLine();
			//Demander le pr�nom
			System.out.println("Quel est votre pr�nom ? ");
			//d�claration d'un objet sc de type Scanner
			Scanner sc = new Scanner(System.in);
			//on affecte ce qui est saisie � l'attribut prenom
			String prenom = sc.next();
			// substring(0,1) permet de recuperer le premier caract�re et toUpperCase() permet de la mettre en majuscule
			// substring (1) permet d'afficher le reste de l'attribut � partir du second caract�re
			String prenomCorrect = prenom.substring(0,1).toUpperCase() + prenom.substring(1);
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom.toUpperCase() + " "+ prenomCorrect);
			sc.close();
	}
	
	public static void exercice4() throws IOException
	{
		String nom = null;
		String prenom = null;
			do
			{
				//Demander le nom
				System.out.println("Quel est votre nom ? ");
				//cr�ation d'un buffer "saisie" puis on lui ajoute InputStreamReader pour �crire des informations
				//system.in permet de saisir au clavier
				BufferedReader saisie = new BufferedReader
					      (new InputStreamReader(System.in));
				//on affecte  ce qui est saisie � un attribut "nom" de type string
				nom = saisie.readLine();
			}
			while(!nom.contains(nom.toUpperCase()));

			do
			{
				//Demander le pr�nom
				System.out.println("Quel est votre pr�nom ? ");
				//d�claration d'un objet sc de type Scanner
				Scanner sc = new Scanner(System.in);
				//on affecte ce qui est saisie � l'attribut prenom
				prenom = sc.next();
			}
			while (!prenom.substring(0,1).contains(prenom.substring(0,1).toUpperCase()));
			
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom + " "+ prenom);
	}
	
	public static void exercice5() throws IOException
	{
			//Demander le nom
			System.out.println("Quel est votre nom ? ");
			//cr�ation d'un buffer "saisie" puis on lui ajoute InputStreamReader pour �crire des informations
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			//on affecte  ce qui est saisie � un attribut "nom" de type string
			String nom = saisie.readLine();
			//Demander le pr�nom
			System.out.println("Quel est votre pr�nom ? ");
			//d�claration d'un objet sc de type Scanner
			Scanner sc = new Scanner(System.in);
			//on affecte ce qui est saisie � l'attribut prenom
			String prenom = sc.next();
			// substring(0,1) permet de recuperer le premier caract�re et toUpperCase() permet de la mettre en majuscule
			// substring (1) permet d'afficher le reste de l'attribut � partir du second caract�re
			String prenomCorrect = prenom.substring(0,1).toUpperCase() + prenom.substring(1);
			//saisie de l'�ge
			System.out.println("Quel est votre ann�e de naissance ? ");
			int annee = Calendar.getInstance().get(Calendar.YEAR);
			int dateNaiSc = saisieEntierScanner();
			System.out.println("Confirmer votre ann�e de naissance : ");
			int dateNaiBuff = saisieEntierBuffer();
			int age = annee - dateNaiBuff;
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom.toUpperCase() + " "+ prenomCorrect + ", tu as " + age + " ans");
			// si on saisit une chaine ou un r�el, on a une erreur de compilation
	}
	
	public static void exercice6() throws IOException
	{
			//Demander le nom
			System.out.println("Quel est votre nom ? ");
			//cr�ation d'un buffer "saisie" puis on lui ajoute InputStreamReader pour �crire des informations
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			//on affecte  ce qui est saisie � un attribut "nom" de type string
			String nom = saisie.readLine();
			//Demander le pr�nom
			System.out.println("Quel est votre pr�nom ? ");
			//d�claration d'un objet sc de type Scanner
			Scanner sc = new Scanner(System.in);
			//on affecte ce qui est saisie � l'attribut prenom
			String prenom = sc.next();
			// substring(0,1) permet de recuperer le premier caract�re et toUpperCase() permet de la mettre en majuscule
			// substring (1) permet d'afficher le reste de l'attribut � partir du second caract�re
			String prenomCorrect = prenom.substring(0,1).toUpperCase() + prenom.substring(1);
			int annee = Calendar.getInstance().get(Calendar.YEAR);
			int dateNaiSc = 0;
			int dateNaiBuff = 0;
			boolean isEntier = false;
			do
			{
				//saisie de l'�ge
				System.out.println("Quel est votre ann�e de naissance ? ");
				//system.in permet de saisir au clavier
				try 
				{
					dateNaiSc = saisieEntierScanner();
					isEntier = true;
				}
				catch (InputMismatchException e) 
				{
					System.out.println("La valeur saisie n'est pas un entier");
					isEntier = false;
				}
			} while(!isEntier);
			do
			{
				System.out.println("Confirmer votre ann�e de naissance : ");
				try 
				{
					dateNaiBuff = saisieEntierBuffer();
					isEntier = true;
				}
				catch (NumberFormatException e) 
				{
					System.out.println("La valeur saisie n'est pas un entier");
					isEntier = false;
				}
			}
			while(!isEntier);
			int age = annee - dateNaiBuff;
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom.toUpperCase() + " "+ prenomCorrect + ", tu as " + age + " ans.");
	}
	
	//exercice 7
	public static double saisieDouble(String message)
	{
		System.out.println(message);
		//d�claration d'un objet sc de type Scanner
		Scanner sc = new Scanner(System.in);
		//on affecte ce qui est saisie � l'attribut nombre
		return sc.nextDouble();
	}
	
	public static void exercice8()
	{
			//Demander le nom
			//on affecte  ce qui est saisie � un attribut "nom" de type string
			String nom = Saisie.saisieStringBuffer("Quel est votre nom ? ");
			//Demander le pr�nom
			//on affecte ce qui est saisie � l'attribut prenom
			String prenom = Saisie.saisieStringBuffer("Quel est votre pr�nom ? ");
			// substring(0,1) permet de recuperer le premier caract�re et toUpperCase() permet de la mettre en majuscule
			// substring (1) permet d'afficher le reste de l'attribut � partir du second caract�re
			String prenomCorrect = prenom.substring(0,1).toUpperCase() + prenom.substring(1);
			int annee = Calendar.getInstance().get(Calendar.YEAR);
			//saisie de l'�ge
			int dateNaiSc = Saisie.saisieEntierScanner("Quel est votre ann�e de naissance ? ");
			int dateNaiBuff = Saisie.saisieEntierBuffer("Confirmer votre ann�e de naissance : ");
			int age = annee - dateNaiBuff;
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom.toUpperCase() + " "+ prenomCorrect + ", tu as " + age + " ans");
	}
	
	public static void exercice9()
	{
			//Demander le nom
			//on affecte  ce qui est saisie � un attribut "nom" de type string
			String nom = Saisie.saisieStringBuffer("Quel est votre nom ? ");
			//Demander le pr�nom
			//on affecte ce qui est saisie � l'attribut prenom
			String prenom = Saisie.saisieStringBuffer("Quel est votre pr�nom ? ");
			// substring(0,1) permet de recuperer le premier caract�re et toUpperCase() permet de la mettre en majuscule
			// substring (1) permet d'afficher le reste de l'attribut � partir du second caract�re
			String prenomCorrect = prenom.substring(0,1).toUpperCase() + prenom.substring(1);
			
			//saisie de l'�ge
			int dateNaiSc = Saisie.saisieEntierScanner("Quel est votre ann�e de naissance ? ");
			int dateNaiBuff = Saisie.saisieEntierBuffer("Confirmer votre ann�e de naissance : ");
			
			LocalDate date = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
			DateTimeFormatter formatComplet = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
			int annee = date.getYear();
			System.out.println("Date d'aujourd'hui " + date.format(formatter));
			int age = annee - dateNaiBuff;
			//affichage de ce qui est saisie
			System.out.println("Bonjour "+ nom.toUpperCase() + " "+ prenomCorrect + ", tu as " + age + " ans");
			System.out.println("Date d'aujourd'hui " + date.format(formatComplet));
	}
	public static int saisieEntierBuffer() throws IOException
	{
		//system.in permet de saisir au clavier
		BufferedReader saisie = new BufferedReader
			      (new InputStreamReader(System.in));
		//on affecte  ce qui est saisie � un attribut "nom" de type string
		 String chaine = saisie.readLine();
		 return Integer.parseInt(chaine);
	}
	
	
	public static int saisieEntierScanner()
	{
		//d�claration d'un objet sc de type Scanner
		Scanner sc = new Scanner(System.in);
		//on affecte ce qui est saisie � l'attribut nombre
		int nombre = sc.nextInt();
		return nombre;
	}
	
}
