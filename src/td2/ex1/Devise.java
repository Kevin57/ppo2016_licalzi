package td2.ex1;

public class Devise implements Comparable<Devise> {
	
	private String nom;
	private double montant;
	
	/** 
	 * accesseurs
	 */
	public double setMontant(double d)
	{
		return montant = d;
	}
	
	public double getMontant()
	{
		return montant;
	}
	
	public String setNom(String n)
	{
		return nom = n;
	}
	
	public String getNom()
	{
		return nom;
	}
	
	/** 
	 * constructeur
	 */
	
	public Devise (String pNom, double pMontant)
	{
		nom = pNom;
		montant = pMontant;
	}
	
	/** 
	 * m�thode qui renvoie un bool�en pour savoir si les devises sont �galesS
	 */
	public boolean equals(Object o)
	{
		Devise d = (Devise)o;
		return this.nom.equalsIgnoreCase(d.nom);
	}
	
	/** 
	 * m�thode qui renvoie les infos de la devise
	 */
	public String toString()
	{
		return this.montant + " " + this.nom;
	}
	
	/** 
	 * m�thode qui renvoie un bool�en pour classer
	 *  les devises par ordre alphab�tique 
	 *  avec collections.sort
	 */
	
	public int compareTo(Devise d)
	{
		int nb = this.nom.compareToIgnoreCase(d.nom);
		return nb;
	}

}
