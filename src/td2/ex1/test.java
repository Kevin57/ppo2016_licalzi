package td2.ex1;

import java.util.Collections;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Devise euros = new Devise("Euros", 20.68);
		Devise dollars = new Devise("Dollars", 5.8);
		Devise moinseuros = new Devise("Euros", 10.68);
		Devise yen = new Devise("Yen", 9.98);
		
		Portefeuille monPortefeuille = new Portefeuille();
		
		monPortefeuille.ajoutDevise(yen);
		monPortefeuille.ajoutDevise(dollars);
		monPortefeuille.ajoutDevise(euros);
		monPortefeuille.afficherPortefeuille();
		monPortefeuille.SortirDevise(moinseuros);
		
		monPortefeuille.afficherPortefeuille();
		Collections.sort(monPortefeuille.getDevises());
		
		monPortefeuille.afficherPortefeuille();
	}

}
