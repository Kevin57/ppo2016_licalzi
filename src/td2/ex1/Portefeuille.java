package td2.ex1;

import java.util.ArrayList;

public class Portefeuille {
	
	private ArrayList<Devise> devises;
	/** 
	 * m�thode qui renvoie les devises de la liste
	 */
	
	public ArrayList<Devise> getDevises()
	{
		return this.devises;
	}
	
	/** 
	 * constructeur
	 */
	public Portefeuille()
	{
		this.devises = new ArrayList<Devise>();
	}
	
	/** 
	 * m�thode qui ajoute les devises dans le portefeuille
	 */
	public void ajoutDevise(Devise d)
	{
			if(d.getMontant() > 0) // si le montant est sup�rieur � 0, on fait l'ajour
			{
				if (getDevises().isEmpty())
				{
					this.devises.add(d);
					System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
				}
				else
				{
					boolean existe = false;
					for(Devise de : getDevises()) // parcours de la liste de devise existante
					{
						if(de.getNom().equals((d.getNom())))// si la devise existe d�j�, on ajoute le montant � celui d�j� existant
						{
							de.setMontant(de.getMontant() + d.getMontant());
							System.out.println(d.getMontant() + " � �t� ajout� au portefeuille");
							existe = true;
						}
					}
					if(!existe)
					{
							devises.add(new Devise(d.getNom(),d.getMontant()));
							System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
					}
				}
			}
			else // si le montant est �gale � 0, on affiche un message
			{
				System.out.println("Le montant doit �tre sup�rieur � 0");
			}
	}
	
	public void SortirDevise(Devise d)
	{
		if(d.getMontant() > 0) // si le montant est sup�rieur � 0, on fait l'ajour
		{
			if (getDevises().isEmpty())
			{
				getDevises().add(d);
				System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
			}
			else
			{
				boolean existe = false;
				for(Devise de : getDevises()) // parcours de la liste de devise existante
				{
					if(de.getNom().contains(d.getNom()))// si la devise existe d�j�, on ajoute le montant � celui d�j� existant
					{
						de.setMontant(de.getMontant() - d.getMontant());
						System.out.println(d.getMontant() + " � �t� retir� au portefeuille");
						existe = true;
					}
				}
				if(!existe)
				{
						System.out.println(d.getNom() + "n'est pas dans le portefeuille");
				}
			}
		}
		else // si le montant est �gale � 0, on affiche un message
		{
			System.out.println("Le montant doit �tre sup�rieur � 0");
		}
	}
	
	/** 
	 * m�thode qui supprime les devises du portefeuille
	 */
	
	public void suppDevise(Devise d)
	{
		boolean isDev = devises.remove(d);
		if(!isDev)
			devises.remove(d);
		else
			System.out.println("La devise n'est pas dans la liste");
	}
	/** 
	 * m�thode qui affiche les devises du portefeuille
	 */
	public void afficherPortefeuille()
	{
		for(Devise de : getDevises()) // parcours de la liste de devise existante
		{
			if(!getDevises().isEmpty())
			{
				System.out.println(de.toString());
			}
			else
			{
				System.out.println("Le portefeuille est vide !");
			}
		}
	}

}
