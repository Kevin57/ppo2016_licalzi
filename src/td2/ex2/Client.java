package td2.ex2;

import td2.ex1.Devise;

public class Client implements Comparable<Client>{

	protected int id;
	protected String nom;
	protected String prenom;
	protected double chiffreA;
	/**
	 * constructeur
	 * @param nom
	 * @param prenom
	 * @param chiffreA
	 */
	public Client(String nom, String prenom, double chiffreA) {
		id++;
		this.nom = nom;
		this.prenom = prenom;
		this.chiffreA = chiffreA;
	}

	/**
	 * 
	 * @ accesseurs de la classe client
	 */
	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public double getChiffreA() {
		return chiffreA;
	}


	public void setChiffreA(double chiffreA) {
		this.chiffreA = (double)chiffreA;
	}


	/** 
	 * m�thode qui renvoie les infos du client
	 */
	public String toString() {
		return "Client nom :" + nom + " prenom :" + prenom + " chiffre d'affaire : " + chiffreA;
	}
	
	/** 
	 * m�thode qui affiche les infos du client
	 */
	public void Affiche() {
		System.out.println(this.toString());
	}
	/** 
	 * m�thode qui compare le chiffre d'affaire pour le classement des clients
	 */
	
	public int compareTo(Client c)
	{
		return (int) (c.chiffreA - this.chiffreA);
	}
	/** 
	 * m�thode qui permet de saisir les infos du client
	 */
	public static Client Saisie()
	{
		String nom = Saisie.saisieStringBuffer("nom du client : ");
		String prenom = Saisie.saisieStringBuffer("Prenom du client :");
		double CA = Saisie.saisieCA("Chiffre d'affaire :");		
		return new Client(nom,prenom,CA);
	}
	
	
	
}
