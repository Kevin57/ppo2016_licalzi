package td2.ex2;

public class ClientPrivilegie extends Client{
	
	private double reduc;
	/** 
	 * constructeur
	 */
	public ClientPrivilegie(String nom, String prenom, double chiffreA, double reduction) {
		super(nom, prenom, chiffreA);
		reduc = reduction;
	}
	
	/** 
	 * m�thode qui renvoie les infos du client privil�gi�
	 */
	public String toString() {
		return "Client nom :" + nom + ", prenom :" + prenom + ", chiffre d'affaire :" + chiffreA + " euros " + "reduction: " + reduc*100 + "%";
	}

}
