package td2.ex2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import td2.ex1.Devise;

public class Clientele {
	
	private ArrayList<Client> lesClients;
	
	/** 
	 * @return accesseur de l'arraylist
	 */
	public ArrayList<Client> getLesClients() {
		return lesClients;
	}
	
	public void setLesClients(ArrayList<Client> lesClients) {
		this.lesClients = lesClients;
	}

	/** 
	 * constructeur
	 */
	public Clientele(){
		// TODO Auto-generated constructor stub
		this.lesClients = new ArrayList<Client>();
	}

	/** 
	 * @param m�thode qui ajoute les clients dans la liste
	 */
	public void add(Client c)
	{
		lesClients.add(c);
		estPrivil�gi�(c.getId());
	}
	/** 
	 * m�thode qui ajoute du chiffre d'affaire aux clients
	 */
	public void addCA(int num, double chiffre)
	{
		for (Client c : lesClients)
		{
			if(chiffre <= 0)
			{
				System.out.println("Erreur :Le montant est inf�rieur ou �gal � 0");
			}
			else
			{
				if(c.getId() == num)
				{
					BigDecimal number = new BigDecimal((chiffre + c.getChiffreA()));
					number = number.setScale(2, RoundingMode.DOWN);
					double dbl = number.doubleValue();
					c.setChiffreA(dbl);
				}
			}
		}
	}
	
	public void affiche()
	{
		for (Client c : lesClients)
		{
				System.out.println(c.toString());
		}
	}
	

	
	public boolean estPrivil�gi�(int id)
	{
		Client c = lesClients.get(id-1);
		boolean privilegie = false;
		if(this.lesClients.contains(c))
		{
			if(c.getChiffreA() < 1000)
			{
				privilegie = false;
			}
			else if(c.getChiffreA() > 1000 && c.getChiffreA() < 3000)
			{
				privilegie = true;
				lesClients.add(new  ClientPrivilegie (c.getNom(), c.getPrenom(),c.getChiffreA(), TypeClient.BonClient.reduction()));
				lesClients.remove(c);
			}
			else if(c.getChiffreA() > 3000 && c.getChiffreA() < 10000)
			{
				privilegie = true;
				lesClients.add(new  ClientPrivilegie (c.getNom(), c.getPrenom(),c.getChiffreA(), TypeClient.ClientExceptionnel.reduction()));
				lesClients.remove(c);
			}
			else
			{
				privilegie = true;
				lesClients.add(new  ClientPrivilegie (c.getNom(), c.getPrenom(),c.getChiffreA(), TypeClient.Vic.reduction()));
				lesClients.remove(c);
			}
		}
		else
		{
			System.out.println("Ce client n'existe pas");
		}
		return privilegie;
	}

}
