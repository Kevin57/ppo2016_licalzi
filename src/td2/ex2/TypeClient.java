package td2.ex2;

public enum TypeClient {
	/** 
	 * on d�finit les �num�rations 
	 */
	Vic,BonClient,ClientExceptionnel;
	
	/** 
	 * m�thode qui renvoie les reductions en fonction du client
	 */
	public double reduction()
	{
		switch(this)
		{
			case BonClient : return 0.05;
			case Vic : return 0.30;
			case ClientExceptionnel : return 0.15;
		}
		return reduction();
	}
	

}
