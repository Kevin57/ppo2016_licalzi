package td3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import td3.FACTORY.DAOFactory;
import td3.FACTORY.Persistance;
import td3.MYSQL.Connexion;
import td3.MYSQL.MYSQLPortefeuilleDAO;

public class test {

	static ArrayList<Portefeuille> listePortefeuille = new ArrayList<Portefeuille>();
	static Portefeuille porteFeuilleSelectionne = null;
	static Portefeuille porteFeuilleSauvegarde = null;

	static String s = null;
	static String nomDevise = null;
	static Double montantDevise = 0.0;
	static Connexion connection = new Connexion();
	static Connection laConnexion = connection.creeConnexion();

	public static void LireFichier(String fichier) {
		try {
			System.out.println("Les portefeuilles sauvegard�s ont �t� charg�s");
			BufferedReader br = new BufferedReader(new FileReader(fichier));
			while ((s = br.readLine()) != null) {
				if (s.contains("PORTEFEUILLE :")) {
					String nomPortefeuille = s.substring(s.indexOf(':') + 2);
					porteFeuilleSauvegarde = new Portefeuille(nomPortefeuille);
				} else if (s.contains(":")) {
					nomDevise = s.substring(0, s.indexOf(':'));
					montantDevise = Double.parseDouble(s.substring(s.indexOf(':') + 2));
					porteFeuilleSauvegarde.ajoutDevise(new Devise(nomDevise), montantDevise);
				} else {
					listePortefeuille.add(porteFeuilleSauvegarde);
				}
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void LireFichierSerialize(String fichier) {
		File f = new File(fichier);
		try {
			System.out.println("Les portefeuilles sauvegard�s ont �t� charg�s");
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(f));
			listePortefeuille = (ArrayList<Portefeuille>) is.readObject();
			is.close();
		} catch (IOException ioe) {
			System.out.println("Probl�me fichier");
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			System.out.println("Probl�me de cast de classe");
			cnfe.printStackTrace();
		}
	}

	public static void main(String[] args) {

		/*
		 * Portefeuille p = new Portefeuille("p1"); Devise euros = new
		 * Devise("euros"); Devise franc = new Devise("franc");
		 * p.ajoutDevise(euros, 40.85); p.ajoutDevise(franc, 56.5);
		 * MYSQLPortefeuilleDAO.getInstance().create(p); p.SortirDevise(euros,
		 * 20.0); MYSQLPortefeuilleDAO.getInstance().update(p);
		 */
		// PortefeuilleDAO.getInstance().delete(p);

		DAOFactory daos = DAOFactory.getDAOFactory(Persistance.MYSQL);
		Devise d = new Devise("yen");
		Portefeuille p = new Portefeuille("p1");
		p.ajoutDevise(d, 40.0);
		listePortefeuille.add(p);
		// daos.getPortefeuilleDAO().create(p);
		// daos.getPortefeuilleDAO().delete(p);
		daos.getContenuDAO().findAll();


	int choix = 0;

	LireFichier("src/td3/fichiersplats.txt");
	LireFichierSerialize("src/td3/portefeuille.bin");
	do
	{
		if (listePortefeuille.size() == 0)
		{
			choix = 1;
		}
		else
		{
			choix = Saisie.saisieEntierBuffer(
					"Pour ajouter un portefeuille, tapez 1\n" +
					"Pour choisir le portefeuille dans lequel on effectue des op�rations, tapez 2\n" +
					"Pour afficher un portefeuille, tapez 3\n"+
					"Pour Sauvegarder un portefeuille, tapez 4\n"
					);
		}
		switch (choix)
		{
			case 1:
				String nomPorteFeuille = Saisie.saisieStringBuffer("Quel est le nom du portefeuille ?");
				listePortefeuille.add(new Portefeuille(nomPorteFeuille));
				daos.getPortefeuilleDAO().create(p);
				break;
			case 2 :
				System.out.println("Liste de portefeuille :");
				for(Portefeuille por : listePortefeuille)
				{
					System.out.println(por.getNom());
				}
				String choixP = Saisie.saisieStringBuffer("Quel est le nom du portefeuille ?");
				int index = -1;
				for(Portefeuille p2 : listePortefeuille)
				{
					if(p2.getNom().equalsIgnoreCase(choixP))
					{
						index = listePortefeuille.indexOf(p2);
						porteFeuilleSelectionne = p2;
					}
				}
				Devise deviseSelectionne = null;
				if (index == -1)
				{
					System.out.println("Ce portefeuille n'existe pas");
				}
				else
				{
					int choixD = 0;
					do
					{
						if(porteFeuilleSelectionne.getDevises() == null)
						{
							choixD = 1;
						}
						else
						{
							choixD = Saisie.saisieEntierBuffer(
							"Pour ajouter de l'argent dans une devise, tapez 1\n" +
							"Pour retirer de l'argent dans une devise, tapez 2\n" +
							"Pour sortir du menu, tapez 3\n"
							);
						}
						
						switch(choixD)
						{
							case 1:
								if (porteFeuilleSelectionne.getDevises() != null)
								{
									System.out.println("Liste de devise :");
									porteFeuilleSelectionne.afficherPortefeuille();
								}
								nomDevise = Saisie.saisieStringBuffer("Quel est le nom de la devise ?");
								montantDevise = Saisie.saisieDouble("Quel est le montant de la devise ?");
								Devise dev = new Devise(nomDevise);
								porteFeuilleSelectionne.ajoutDevise(dev, montantDevise);
								daos.getDeviseDAO().create(dev);
								daos.getContenuDAO().create(porteFeuilleSelectionne);
								if(porteFeuilleSelectionne.getDevises().containsKey(deviseSelectionne))
								{
									porteFeuilleSelectionne.ajoutDevise(deviseSelectionne, montantDevise);
								}
							break;
							case 2:
								if (porteFeuilleSelectionne.getDevises() != null)
								{
									System.out.println("Liste de devise :");
									porteFeuilleSelectionne.afficherPortefeuille();
								}
								nomDevise = Saisie.saisieStringBuffer("Quel est le nom de la devise ?");
								deviseSelectionne = new Devise(nomDevise);
								montantDevise = Saisie.saisieDouble("Quel est le montant de la devise ?");
								if(porteFeuilleSelectionne.getDevises().containsKey(deviseSelectionne))
								{
									porteFeuilleSelectionne.SortirDevise(deviseSelectionne, montantDevise);
									daos.getContenuDAO().update(porteFeuilleSelectionne);
								}
							break;
							default:
								choixD = 0;
								porteFeuilleSelectionne = null;
							break;
						}
					}
					while (choixD != 0);
					
				}
				break;
			case 3 :
				if(porteFeuilleSelectionne == null)
				{
					System.out.println("Liste de portefeuille :");
					for(Portefeuille po : listePortefeuille)
					{
						System.out.println(po.getNom());
					}
					choixP = Saisie.saisieStringBuffer("Quel est le nom du portefeuille ?");
					for(Portefeuille p2 : listePortefeuille)
					{
						if(p2.getNom().equalsIgnoreCase(choixP))
						{
							index = listePortefeuille.indexOf(p2);
							porteFeuilleSelectionne = p2;
							porteFeuilleSelectionne.afficherPortefeuille();
						}
					}
				}
				else
				{
					porteFeuilleSelectionne.afficherPortefeuille();
					porteFeuilleSelectionne = null;
				}
			break;
			case 4:
					try { 
						
						BufferedWriter bw = new BufferedWriter(new FileWriter("src/td3/fichiersplats.txt"));
						for(Portefeuille p2 : listePortefeuille)
						{
							bw.write("PORTEFEUILLE : "+ p2.getNom()); 
							bw.newLine();
							for (HashMap.Entry<Devise, Double> entry : p2.getDevises().entrySet())
							{
								bw.write(entry.getKey() + " : " + entry.getValue());
								bw.newLine();
							}
							bw.write("FIN PORTEFEUILLE"); 
							bw.newLine(); 
						}
						bw.close(); 
						} catch (IOException ioe) 
						{ 
							ioe.printStackTrace(); 
						}
					
					 	try 
					 	{ 
						 		ObjectOutputStream os = new ObjectOutputStream( new FileOutputStream(new File("src/td3/portefeuille.bin"))); 
						 		os.writeObject(listePortefeuille);
						 		os.close(); 
					 	} catch (IOException ioe)
					 	{ 
					 		ioe.printStackTrace(); 
					 	}
			break;

		}
	}
	while(choix !=0);
}

}