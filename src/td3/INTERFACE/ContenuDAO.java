package td3.INTERFACE;


public interface ContenuDAO<T> extends DAO<T> {
	public abstract T getById(int id);

	public abstract void findAll();
}
