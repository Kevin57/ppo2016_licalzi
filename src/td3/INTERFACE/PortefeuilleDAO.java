package td3.INTERFACE;

public interface PortefeuilleDAO<T> extends DAO<T> {

	public abstract T getByNom(String nom);
	public abstract void findAll();

}
