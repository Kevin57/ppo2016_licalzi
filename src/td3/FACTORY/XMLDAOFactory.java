package td3.FACTORY;

import td3.INTERFACE.ContenuDAO;
import td3.INTERFACE.DeviseDAO;
import td3.INTERFACE.PortefeuilleDAO;
import td3.XML.XMLContenuDAO;
import td3.XML.XMLDeviseDAO;
import td3.XML.XMLPortefeuilleDAO;

public class XMLDAOFactory extends DAOFactory {
	@Override
	public PortefeuilleDAO getPortefeuilleDAO() {
		return XMLPortefeuilleDAO.getInstance();
	}

	@Override
	public DeviseDAO getDeviseDAO() {
		return XMLDeviseDAO.getInstance();
	}

	@Override
	public ContenuDAO getContenuDAO() {
		return XMLContenuDAO.getInstance();
	}
}
