package td3.FACTORY;

import td3.INTERFACE.ContenuDAO;
import td3.INTERFACE.DeviseDAO;
import td3.INTERFACE.PortefeuilleDAO;
import td3.MYSQL.MYSQLContenuDAO;
import td3.MYSQL.MYSQLDeviseDAO;
import td3.MYSQL.MYSQLPortefeuilleDAO;

public class MySQLDAOFactory extends DAOFactory {
		@Override
		public PortefeuilleDAO getPortefeuilleDAO() {
			return MYSQLPortefeuilleDAO.getInstance();
		}

		@Override
		public DeviseDAO getDeviseDAO() {
			return MYSQLDeviseDAO.getInstance();
		}

		@Override
		public ContenuDAO getContenuDAO() {
			return MYSQLContenuDAO.getInstance();
		}

}
