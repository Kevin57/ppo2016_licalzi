package td3.FACTORY;

import td3.INTERFACE.ContenuDAO;
import td3.INTERFACE.DeviseDAO;
import td3.INTERFACE.PortefeuilleDAO;

public abstract class DAOFactory {
	public static DAOFactory getDAOFactory(Persistance cible) {
		DAOFactory daoF = null;
		switch (cible) {
		case MYSQL:
			daoF = new MySQLDAOFactory();
			break;
		case XML:
			daoF = new XMLDAOFactory();
			break;
		}
		return daoF;
	}

	public abstract PortefeuilleDAO getPortefeuilleDAO();
	public abstract DeviseDAO getDeviseDAO();
	public abstract ContenuDAO getContenuDAO();
}