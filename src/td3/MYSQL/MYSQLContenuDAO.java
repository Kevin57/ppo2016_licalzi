package td3.MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import td3.Devise;
import td3.Portefeuille;
import td3.FACTORY.DAOFactory;
import td3.FACTORY.Persistance;
import td3.INTERFACE.ContenuDAO;
import td3.INTERFACE.DAO;
import td3.INTERFACE.PortefeuilleDAO;
public class MYSQLContenuDAO implements DAO<Portefeuille>, ContenuDAO<Portefeuille> {
	Connexion connection = new Connexion();
	Connection laConnexion = connection.creeConnexion();
	/** Instance unique non pr�initialis�e */
	private static MYSQLContenuDAO INSTANCE = null;
	
	/** Point d'acc�s pour l'instance unique du singleton */
	public static synchronized MYSQLContenuDAO getInstance()
	{			
		if (INSTANCE == null)
		{ 	
			INSTANCE = new MYSQLContenuDAO();	
		}
		return INSTANCE;
	}

	@Override
	public Portefeuille getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("insert into Contenu VALUES (?,?,?)");
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{
				requete.setInt(1,p.getID());
				requete.setInt(2,entry.getKey().getID());
				requete.setDouble(3,entry.getValue().doubleValue());
				requete.executeUpdate();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	@Override
	public void findAll() {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Contenu");
			ResultSet res = requete.executeQuery();
			while(res.next())
			{
				System.out.println("id portefeuille : "+res.getInt(1));
				System.out.println("id devise : "+res.getInt(2));
				System.out.println("montant : "+res.getDouble(3));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void update(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("UPDATE Contenu SET montant = ? WHERE id_port = ? AND id_devise = ?");
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{
				requete.setDouble(1,entry.getValue().doubleValue());
				requete.setInt(2,p.getID());
				requete.setInt(3,entry.getKey().getID());
				requete.executeUpdate();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void delete(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Contenu WHERE id_port=?");
			requete.setInt(1, p.getID());
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{
				PreparedStatement req = laConnexion.prepareStatement("SELECT id from Devise WHERE nom=?");
				req.setString(1, (entry.getKey().getNom()));
				ResultSet res = req.executeQuery();
				if(res.next())
				{
					entry.getKey().setID(res.getInt("id"));
				}
				MYSQLDeviseDAO.getInstance().delete(entry.getKey());
			}
			requete.executeUpdate();
			System.out.println("Le contenu du portefeuille est supprim� !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		
	}

}
