package td3.MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import td3.Devise;
import td3.Portefeuille;
import td3.INTERFACE.DAO;
import td3.INTERFACE.PortefeuilleDAO;

public class MYSQLPortefeuilleDAO implements DAO<Portefeuille>, PortefeuilleDAO<Portefeuille>{
	Connexion connection = new Connexion();
	Connection laConnexion = connection.creeConnexion();
	/** Instance unique non pr�initialis�e */
	private static MYSQLPortefeuilleDAO INSTANCE = null;
	
	/** Point d'acc�s pour l'instance unique du singleton */
	public static synchronized MYSQLPortefeuilleDAO getInstance()
	{			
		if (INSTANCE == null)
		{ 	INSTANCE = new MYSQLPortefeuilleDAO();	
		}
		return INSTANCE;
	}

	@Override
	public Portefeuille getById(int id) {
		Portefeuille p = null;
		try
		{
				
			PreparedStatement requete = laConnexion.prepareStatement("select * from Portefeuille where id=?");
			requete.setInt(1, p.getID());
			ResultSet res = requete.executeQuery();
			while (res.next()) 
			{
				p = new Portefeuille(res.getString("nom"));
			}
			return p;
		}
		catch(SQLException e)
		{
			System.out.println("SQL ERROR:"+e.getMessage());
			return null;
		}
	}
	

	@Override
	public void create(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("insert into Portefeuille(nom) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, p.getNom());
			requete.executeUpdate();
			ResultSet res = requete.getGeneratedKeys();
			int cle = 0;
			if(res.next()) 
			{
				cle	 = res.getInt(1); 
				p.setID(cle);
			}
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{					
				MYSQLDeviseDAO.getInstance().create(entry.getKey());
			}
			MYSQLContenuDAO.getInstance().create(p);
			System.out.println("ajout du portefeuille : ok !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur SQL:"+e.getMessage());
		}
		
	}

	@Override
	public void delete(Portefeuille p) {
		try {
			PreparedStatement req = laConnexion.prepareStatement("SELECT id from Portefeuille WHERE nom=?");
			req.setString(1, p.getNom());
			ResultSet res = req.executeQuery();
			if(res.next())
			{
				p.setID(res.getInt("id"));
			}
			MYSQLContenuDAO.getInstance().delete(p);
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Portefeuille WHERE id=?");
			requete.setInt(1, p.getID());
			requete.executeUpdate();
			System.out.println("Suppression de " + p.getNom() + " : ok !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}

	@Override
	public void findAll() {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Portefeuille");
			ResultSet res = requete.executeQuery();
			while(res.next())
			{
				System.out.println("id portefeuille : "+res.getInt(1));
				System.out.println("Nom : "+res.getString(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	@Override
	public void update(Portefeuille p) {
		PreparedStatement req;
		try {
			req = laConnexion.prepareStatement("SELECT id from Portefeuille WHERE nom=?");
			req.setString(1, p.getNom());
			ResultSet res = req.executeQuery();
			if(res.next())
			{
				p.setID(res.getInt("id"));
			}
			MYSQLContenuDAO.getInstance().update(p);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public Portefeuille getByNom(String nom) {
		Portefeuille p = null;
		try
		{
			PreparedStatement requete = laConnexion.prepareStatement("select * from Portefeuille where nom=?");
			requete.setString(1, p.getNom());
			ResultSet res = requete.executeQuery();
			while (res.next()) 
			{
				p = new Portefeuille(res.getString("nom"));
				p.setID(res.getInt("id"));
			}
			return p;
		}
		catch(SQLException e)
		{
			System.out.println("SQL ERROR:"+e.getMessage());
			return null;
		}

	}

}
