package td3.MYSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import td3.Devise;
import td3.INTERFACE.DAO;
import td3.INTERFACE.DeviseDAO;
import td3.INTERFACE.PortefeuilleDAO;

public class MYSQLDeviseDAO implements DAO<Devise>, DeviseDAO<Devise>{
	Connexion connection = new Connexion();
	Connection laConnexion = connection.creeConnexion();
	/** Instance unique non pr�initialis�e */
	private static MYSQLDeviseDAO INSTANCE = null;
	
	/** Point d'acc�s pour l'instance unique du singleton */
	public static synchronized MYSQLDeviseDAO getInstance()
	{			
		if (INSTANCE == null)
		{ 	INSTANCE = new MYSQLDeviseDAO();	
		}
		return INSTANCE;
	}

	@Override
	public Devise getById(int id) {
		Devise d = null;
		try
		{
				
			PreparedStatement requete = laConnexion.prepareStatement("select * from Portefeuille where id=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while (res.next()) 
			{
				d = new Devise(res.getString("nom"));
			}
			System.out.println("Ajout de la devise : ok !");
			return d;
		}
		catch(SQLException e)
		{
			System.out.println("SQL ERROR:"+e.getMessage());
			return null;
		}
	}

	@Override
	public void create(Devise d) {
			try {
			PreparedStatement req = laConnexion.prepareStatement("SELECT nom from Devise WHERE nom=?");
			req.setString(1, d.getNom());
			ResultSet res = req.executeQuery();
			if(res.next())
			{
				d.setID(res.getInt("id"));
			}
			else
			{
				PreparedStatement requete = laConnexion.prepareStatement("insert into Devise(nom) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				requete.setString(1, d.getNom());
				int nbLignes = requete.executeUpdate();
				ResultSet result = requete.getGeneratedKeys();
				int cle = 0;
				if(result.next()) 
				{
					cle	 = result.getInt(1); 
					d.setID(cle);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur SQL:"+e.getMessage());
		}
		
	}

	@Override
	public void update(Devise objet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Devise d) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Devise WHERE id=?");
			requete.setInt(1, d.getID());
			requete.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur SQL:"+e.getMessage());
		}
		
	}

	@Override
	public Devise getByNom(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findAll() {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Devise");
			ResultSet res = requete.executeQuery();
			while(res.next())
			{
				System.out.println("id devise : "+res.getInt(1));
				System.out.println("Nom : "+res.getString(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}

}
