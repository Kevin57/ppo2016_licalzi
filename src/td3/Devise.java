package td3;

import java.io.Serializable;

public class Devise implements Serializable, Comparable<Devise> {
	
	private static final long serialVersionUID = 2002210902577820191L;
	private int id;
	private String nom;
	
	/** 
	 * accesseurs
	 */
	public int getID() {
		return id;
	}
	public int setID(int nb) {
		return id = nb;
	}
	public String setNom(String n)
	{
		return nom = n;
	}
	
	public String getNom()
	{
		return nom;
	}
	
	/** 
	 * constructeur
	 */
	
	public Devise (String pNom)
	{
		this.id++;
		nom = pNom;
	}
	
	/** 
	 * m�thode qui renvoie un bool�en pour savoir si les devises sont �galesS
	 */
	public boolean equals(Object o)
	{
		Devise d = (Devise)o;
		return this.nom.equalsIgnoreCase(d.nom);
	}
	
	/** 
	 * m�thode qui renvoie les infos de la devise
	 */
	public String toString()
	{
		return this.nom;
	}
	
	/** 
	 * m�thode qui renvoie un bool�en pour classer
	 *  les devises par ordre alphab�tique 
	 *  avec collections.sort
	 */
	
	public int compareTo(Devise d)
	{
		int nb = this.nom.compareToIgnoreCase(d.nom);
		return nb;
	}
	
	public int hashCode()
	{
		return this.nom.hashCode();
	}

}
