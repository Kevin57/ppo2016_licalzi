package td3.XML;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import td3.Devise;
import td3.Portefeuille;
import td3.INTERFACE.DAO;
import td3.INTERFACE.PortefeuilleDAO;
import td3.MYSQL.Connexion;
import td3.MYSQL.MYSQLContenuDAO;
import td3.MYSQL.MYSQLDeviseDAO;
import td3.MYSQL.MYSQLPortefeuilleDAO;

public class XMLPortefeuilleDAO implements DAO<Portefeuille>, PortefeuilleDAO<Portefeuille>{
	Connexion connection = new Connexion();
	Connection laConnexion = connection.creeConnexion();
	/** Instance unique non pr�initialis�e */
	private static XMLPortefeuilleDAO INSTANCE = null;
	
	/** Point d'acc�s pour l'instance unique du singleton */
	public static synchronized XMLPortefeuilleDAO getInstance()
	{			
		if (INSTANCE == null)
		{ 	
			INSTANCE = new XMLPortefeuilleDAO();	
		}
		return INSTANCE;
	}

	@Override
	public Portefeuille getById(int id) {
		Portefeuille p = null;
		try
		{
				
			PreparedStatement requete = laConnexion.prepareStatement("select * from portefeuille where id=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while (res.next()) 
			{
				p = new Portefeuille(res.getString("nom"));
			}
			return p;
		}
		catch(SQLException e)
		{
			System.out.println("SQL ERROR:"+e.getMessage());
			return null;
		}
	}
	

	@Override
	public void create(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("insert into Portefeuille(nom) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, p.getNom());
			int nbLignes = requete.executeUpdate();
			ResultSet res = requete.getGeneratedKeys();
			int cle = 0;
			if(res.next()) 
			{
				cle	 = res.getInt(1); 
				p.setID(cle);
			}
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{					
				MYSQLDeviseDAO.getInstance().create(entry.getKey());
			}
			MYSQLContenuDAO.getInstance().create(p);
			System.out.println("ajout du portefeuille : ok !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur SQL:"+e.getMessage());
		}
		
	}

	@Override
	public void delete(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Portefeuille WHERE id=?");
			requete.setInt(1, p.getID());
			requete.executeUpdate();
			MYSQLContenuDAO.getInstance().delete(p);
			System.out.println("Suppression de " + p.getNom() + " : ok !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}


	@Override
	public void update(Portefeuille p) {
		MYSQLContenuDAO.getInstance().update(p);
	}

	@Override
	public Portefeuille getByNom(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
