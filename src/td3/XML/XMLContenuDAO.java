package td3.XML;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import td3.Devise;
import td3.Portefeuille;
import td3.INTERFACE.ContenuDAO;
import td3.INTERFACE.DAO;
import td3.INTERFACE.PortefeuilleDAO;
import td3.MYSQL.Connexion;
public class XMLContenuDAO implements DAO<Portefeuille>, ContenuDAO<Portefeuille> {
	Connexion connection = new Connexion();
	Connection laConnexion = connection.creeConnexion();
	/** Instance unique non pr�initialis�e */
	private static XMLContenuDAO INSTANCE = null;
	
	/** Point d'acc�s pour l'instance unique du singleton */
	public static synchronized XMLContenuDAO getInstance()
	{			
		if (INSTANCE == null)
		{ 	
			INSTANCE = new XMLContenuDAO();	
		}
		return INSTANCE;
	}

	@Override
	public Portefeuille getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("insert into Contenu VALUES (?,?,?)");
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{
				requete.setInt(1,p.getID());
				requete.setInt(2,entry.getKey().getID());
				requete.setDouble(3,entry.getValue().doubleValue());
				requete.executeUpdate();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void update(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("UPDATE Contenu SET montant = ? WHERE id_port = ? AND id_devise = ?");
			for (HashMap.Entry<Devise, Double> entry : p.getDevises().entrySet())
			{
				requete.setDouble(1,entry.getValue().doubleValue());
				requete.setInt(2,p.getID());
				requete.setInt(3,entry.getKey().getID());
				requete.executeUpdate();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void delete(Portefeuille p) {
		try {
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Contenu WHERE id_port=?");
			requete.setInt(1, p.getID());
			requete.executeUpdate();
			System.out.println("Le contenu du portefeuille est supprim� !");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		
	}

}
