package td3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Saisie {
	
	private static Scanner sc;

	public static int saisieEntierBuffer(String message)
	{
		boolean isEntier = false;
		int nb = 0;
		do
		{
			System.out.println(message);
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			try {
				nb = Integer.parseInt(saisie.readLine());
				isEntier = true;
			} catch (NumberFormatException | IOException e) {
				System.out.println("La valeur saisie n'est pas un entier");
				isEntier = false;
			}
		}
		while(!isEntier);
		return nb;
	}
	
	public static int saisieEntierBuffer()
	{
		boolean isEntier = false;
		int nb = 0;
		do
		{
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			try {
				nb = Integer.parseInt(saisie.readLine());
				isEntier = true;
			} catch (NumberFormatException | IOException e) {
				System.out.println("La valeur saisie n'est pas un entier");
				isEntier = false;
			}
		}
		while(!isEntier);
		return nb;
	}
	
	public static String saisieStringBuffer(String message)
	{
		String chaine = "";
		boolean estChaine = false;
		do
		{
			System.out.println(message);
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			try {
				chaine = saisie.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(isChaine(chaine))
			{
				estChaine = true;
			}
			else
			{
				estChaine = false;
				System.out.println("La valeur saisie n'est pas une lettre");
			}
		}
		while(!estChaine);
		return chaine;
	}
	
	public static String saisieStringBuffer()
	{
		String chaine = "";
		boolean estChaine = false;
		do
		{
			//system.in permet de saisir au clavier
			BufferedReader saisie = new BufferedReader
				      (new InputStreamReader(System.in));
			try {
				chaine = saisie.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(isChaine(chaine))
			{
				estChaine = true;
			}
			else
			{
				estChaine = false;
				System.out.println("La valeur saisie n'est pas une lettre");
			}
		}
		while(!estChaine);
		return chaine;
	}
	
	//exercice 7
	public static double saisieDouble(String message)
	{
		double nb = 0;
		boolean isDouble = false;
		do
		{
			System.out.println(message);
			sc = new Scanner(System.in);
			//system.in permet de saisir au clavier
			try 
			{
				nb = sc.nextDouble();
				isDouble = true;
			}
			catch (InputMismatchException e) 
			{
				System.out.println("La valeur saisie n'est pas un r�el");
				isDouble = false;
			}
		} while(!isDouble);
		return nb;
	}
	
	public static double saisieDouble()
	{
		double nb = 0;
		boolean isDouble = false;
		do
		{
			sc = new Scanner(System.in);
			//system.in permet de saisir au clavier
			try 
			{
				nb = sc.nextDouble();
				isDouble = true;
			}
			catch (InputMismatchException e) 
			{
				System.out.println("La valeur saisie n'est pas un r�el");
				isDouble = false;
			}
		} while(!isDouble);
		return nb;
	}
	
	public static int saisieEntierScanner(String message)
	{
		int nb = 0;
		boolean isEntier = false;
		do
		{
			System.out.println(message);
			//system.in permet de saisir au clavier
			try 
			{
				sc = new Scanner(System.in);
				nb = sc.nextInt();
				isEntier = true;
			}
			catch (InputMismatchException e) 
			{
				System.out.println("La valeur saisie n'est pas un entier");
				isEntier = false;
			}
		} while(!isEntier);
		return nb;
	}
	
	public static int saisieEntierScanner()
	{
		int nb = 0;
		boolean isEntier = false;
		do
		{
			//system.in permet de saisir au clavier
			try 
			{
				sc = new Scanner(System.in);
				nb = sc.nextInt();
				isEntier = true;
			}
			catch (InputMismatchException e) 
			{
				System.out.println("La valeur saisie n'est pas un entier");
				isEntier = false;
			}
		} while(!isEntier);
		return nb;
	}

	
	public static boolean isChaine(String chaine)
	{
		char [] s = chaine.toCharArray();
		boolean testChaine= false;

		for (int i=0; i<s.length;i++)
		{
			if(!Character.isDigit(s[i]) )
				testChaine= true;
		}
		return testChaine;
	}
	

}
