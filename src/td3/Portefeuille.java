package td3;

import java.io.Serializable;
import java.util.HashMap;

public class Portefeuille implements Serializable{
	
	private static final long serialVersionUID = -493717925895036187L;
	private String nom;
	private int id;
	
	public String getNom() {
		return nom;
	}
	public int getID() {
		return id;
	}
	public int setID(int nb) {
		return id = nb;
	}
	private HashMap<Devise, Double> devises;
	/** 
	 * m�thode qui renvoie les devises de la liste
	 */
	
	public HashMap<Devise, Double> getDevises()
	{
		return this.devises;
	}
	
	/** 
	 * constructeur
	 */
	public Portefeuille(String nom)
	{
		this.id++;
		this.nom = nom;
		this.devises = new HashMap<Devise,Double>();
	}
	
	public Portefeuille()
	{
		this.id++;
		this.nom = null;
		this.devises = new HashMap<Devise,Double>();
	}
	
	/** 
	 * m�thode qui ajoute les devises dans le portefeuille
	 */
	public void ajoutDevise(Devise d, Double montant)
	{
			if(montant > 0) // si le montant est sup�rieur � 0, on fait l'ajout
			{
				if (getDevises().isEmpty())
				{
					this.devises.put(d,montant);
					System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
				}
				else
				{
					if(this.devises.containsKey(d))// si la devise existe d�j�, on ajoute le montant � celui d�j� existant
					{
						double ancienMontant = getDevises().get(d);
						getDevises().put(d, ancienMontant + montant);
						System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
					}
					else
					{
						this.devises.put(d, montant);
						System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
					}
				}
			}
			else // si le montant est �gale � 0, on affiche un message
			{
				this.devises.put(d,montant);
				System.out.println(d.getNom() + " � �t� ajout� au portefeuille");
			}
	}
	
	public void SortirDevise(Devise d, Double montant)
	{
		if(montant > 0) // si le montant est sup�rieur � 0, on fait l'ajout
		{
			if(this.devises.containsKey(d))// si la devise existe d�j�, on soustrait le montant � celui d�j� existant
			{
				if (getDevises().get(d) - montant >= 0)
				{
						double ancienMontant = getDevises().get(d);
						getDevises().put(d, ancienMontant - montant);
						System.out.println(montant + " " + d.getNom() + " a �t� retir� au portefeuille");
				}
				else
				{
					System.out.println("le montant � enlever est sup�rieur au montant actuel de la devise, op�ration impossible");
				}
			}
			else
			{
					System.out.println(d.getNom() + " n'est pas dans le portefeuille");
			}
		}
		else // si le montant est �gale � 0, on affiche un message
		{
			System.out.println("Le montant doit �tre sup�rieur � 0");
		}
	}
	
	/** 
	 * m�thode qui supprime les devises du portefeuille
	 */
	
	public void suppDevise(Devise d)
	{
		boolean isDev = this.devises.remove(d) == null;
		if(!isDev)
		{
			devises.remove(d);
			System.out.println("La devise " + d.getNom() + " a �t� supprim�");
		}
		else
			System.out.println("La devise n'est pas dans le portefeuille");
	}
	/** 
	 * m�thode qui affiche les devises du portefeuille
	 */
	public void afficherPortefeuille()
	{
		for(Devise de : this.devises.keySet()) // parcours de la liste de devise existante
		{
			if(!getDevises().isEmpty())
			{
				System.out.println(de.toString() + " " + this.devises.get(de));
			}
			else
			{
				System.out.println("Le portefeuille est vide !");
			}
		}
	}
	

}
